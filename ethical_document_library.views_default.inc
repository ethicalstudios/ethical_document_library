<?php
/**
 * @file
 * ethical_document_library.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ethical_document_library_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'open_ethical_document_library';
  $view->description = '';
  $view->tag = 'openethical';
  $view->base_table = 'search_api_index_openethical_document_library';
  $view->human_name = 'Open Ethical Document Library';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'Your search does not have any matches. Please adjust your search, or <a href="?">view all documents</a>.';
  $handler->display->display_options['empty']['area']['format'] = 'panopoly_wysiwyg_text';
  /* Field: Indexed File: URL */
  $handler->display->display_options['fields']['url']['id'] = 'url';
  $handler->display->display_options['fields']['url']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['url']['field'] = 'url';
  $handler->display->display_options['fields']['url']['label'] = '';
  $handler->display->display_options['fields']['url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['url']['link_to_entity'] = 0;
  /* Field: Indexed File: Image */
  $handler->display->display_options['fields']['field_featured_image']['id'] = 'field_featured_image';
  $handler->display->display_options['fields']['field_featured_image']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['field_featured_image']['field'] = 'field_featured_image';
  $handler->display->display_options['fields']['field_featured_image']['label'] = '';
  $handler->display->display_options['fields']['field_featured_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_featured_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_featured_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_featured_image']['settings'] = array(
    'image_style' => 'portrait',
    'image_link' => '',
  );
  /* Field: Indexed File: File name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['path'] = '[url]';
  $handler->display->display_options['fields']['name']['element_type'] = '0';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_entity'] = 0;
  /* Field: Indexed File: Author */
  $handler->display->display_options['fields']['field_file_author']['id'] = 'field_file_author';
  $handler->display->display_options['fields']['field_file_author']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['field_file_author']['field'] = 'field_file_author';
  $handler->display->display_options['fields']['field_file_author']['label'] = '';
  $handler->display->display_options['fields']['field_file_author']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_file_author']['element_class'] = 'author';
  $handler->display->display_options['fields']['field_file_author']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_author']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_file_author']['hide_empty'] = TRUE;
  /* Field: Indexed File: Description, summary or abstract */
  $handler->display->display_options['fields']['field_file_description']['id'] = 'field_file_description';
  $handler->display->display_options['fields']['field_file_description']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['field_file_description']['field'] = 'field_file_description';
  $handler->display->display_options['fields']['field_file_description']['label'] = '';
  $handler->display->display_options['fields']['field_file_description']['element_type'] = '0';
  $handler->display->display_options['fields']['field_file_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_description']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_file_description']['hide_empty'] = TRUE;
  /* Field: File: Rendered File */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_file';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'link';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  /* Field: Indexed File: Category */
  $handler->display->display_options['fields']['field_featured_categories']['id'] = 'field_featured_categories';
  $handler->display->display_options['fields']['field_featured_categories']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['field_featured_categories']['field'] = 'field_featured_categories';
  $handler->display->display_options['fields']['field_featured_categories']['label'] = '';
  $handler->display->display_options['fields']['field_featured_categories']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_featured_categories']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_featured_categories']['list']['separator'] = '||';
  $handler->display->display_options['fields']['field_featured_categories']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_featured_categories']['view_mode'] = 'full';
  /* Field: Indexed File: Country */
  $handler->display->display_options['fields']['field_country']['id'] = 'field_country';
  $handler->display->display_options['fields']['field_country']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['field_country']['field'] = 'field_country';
  $handler->display->display_options['fields']['field_country']['label'] = '';
  $handler->display->display_options['fields']['field_country']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_country']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_country']['list']['separator'] = '||';
  $handler->display->display_options['fields']['field_country']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_country']['view_mode'] = 'full';
  /* Field: Indexed File: Date published */
  $handler->display->display_options['fields']['field_file_published_date']['id'] = 'field_file_published_date';
  $handler->display->display_options['fields']['field_file_published_date']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['field_file_published_date']['field'] = 'field_file_published_date';
  $handler->display->display_options['fields']['field_file_published_date']['label'] = '';
  $handler->display->display_options['fields']['field_file_published_date']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_file_published_date']['element_class'] = 'date';
  $handler->display->display_options['fields']['field_file_published_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_published_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_file_published_date']['settings'] = array(
    'format_type' => 'panopoly_day',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Project: Title (indexed) */
  $handler->display->display_options['fields']['field_project_title']['id'] = 'field_project_title';
  $handler->display->display_options['fields']['field_project_title']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['field_project_title']['field'] = 'field_project_title';
  /* Field: Indexed File: Project */
  $handler->display->display_options['fields']['field_project']['id'] = 'field_project';
  $handler->display->display_options['fields']['field_project']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['field_project']['field'] = 'field_project';
  $handler->display->display_options['fields']['field_project']['label'] = '';
  $handler->display->display_options['fields']['field_project']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_project']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_project']['list']['separator'] = '||';
  $handler->display->display_options['fields']['field_project']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_project']['view_mode'] = 'full';
  /* Field: Country: Label (indexed) */
  $handler->display->display_options['fields']['field_country_name']['id'] = 'field_country_name';
  $handler->display->display_options['fields']['field_country_name']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['field_country_name']['field'] = 'field_country_name';
  $handler->display->display_options['fields']['field_country_name']['link_to_entity'] = 0;
  /* Field: Indexed File: Document type */
  $handler->display->display_options['fields']['field_oe_document_type']['id'] = 'field_oe_document_type';
  $handler->display->display_options['fields']['field_oe_document_type']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['fields']['field_oe_document_type']['field'] = 'field_oe_document_type';
  $handler->display->display_options['fields']['field_oe_document_type']['label'] = '';
  $handler->display->display_options['fields']['field_oe_document_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_oe_document_type']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_oe_document_type']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_oe_document_type']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_oe_document_type']['bypass_access'] = 0;
  /* Sort criterion: Indexed File: Date published */
  $handler->display->display_options['sorts']['field_file_published_date']['id'] = 'field_file_published_date';
  $handler->display->display_options['sorts']['field_file_published_date']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['sorts']['field_file_published_date']['field'] = 'field_file_published_date';
  $handler->display->display_options['sorts']['field_file_published_date']['order'] = 'DESC';
  /* Sort criterion: Indexed File: Timestamp */
  $handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_openethical_document_library';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );

  /* Display: Document library */
  $handler = $view->new_display('panel_pane', 'Document library', 'panel_pane_1');
  $handler->display->display_options['pane_title'] = 'Document library';
  $handler->display->display_options['pane_category']['name'] = 'Admin';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['inherit_panels_path'] = '1';
  $export['open_ethical_document_library'] = $view;

  return $export;
}
